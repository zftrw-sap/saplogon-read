var fs = require('fs');
var path = require('path');
var ini = require('ini');
var xmldoc = require('xmldoc');


function readFromIniFile(filepath, systemId) {
  if (!fs.existsSync(filepath)) {
    return null;
  }

  var fileContent = fs.readFileSync(filepath, 'utf-8');
  var cfg = ini.parse(fileContent); 

  var itemKey = Object.keys(cfg.MSSysName).find(function(key) {
    return cfg.MSSysName[key] === systemId;
  });

  if (!itemKey) {
    return null;
  }

  return {
    server: cfg.Server[itemKey],
    database: cfg.Database[itemKey],
    description: cfg.Description[itemKey],
  };
}

function readFromXmlFile(filepath, systemId) {
  if (!fs.existsSync(filepath))
    return null;

  var fileContent = fs.readFileSync(filepath, 'utf-8');
  var document = new xmldoc.XmlDocument(fileContent);
  var root = document.childNamed('Landscape');
  if (document.name !== 'Landscape')
    return null;
  var services = document.childNamed('Services');
  if (!services)
    return null;
  var service = services.childWithAttribute('systemid', systemId);
  if (!service)
    return null;

  try {
    var server;
    var port;
    
    description = service.attr.name;
    if (service.attr.msid) {
      var messageServers = document.childNamed('Messageservers');
      var messageServer = messageServers.childWithAttribute('uuid', service.attr.msid);
      server = messageServer.attr.host;
      port = messageServer.attr.port;
    } else {
      var serverInfo = service.attr.server.split(':');
      server = serverInfo[0];
      port = serverInfo[1];
    }

    var database = port.substr(-2);
    return {
      server: server,
      database: database,
      description: service.attr.name,
    };
  }
  catch (err) {
    return null;
  }
}

module.exports = function(systemId) {
  var LOCALAPPDATA = process.env.LOCALAPPDATA || '';
  var APPDATA = process.env.APPDATA || '';

  var config =
    readFromIniFile(path.join(LOCALAPPDATA, 'SAP/Common/saplogon.ini'), systemId) ||
    readFromIniFile(path.join(APPDATA, 'SAP/Common/saplogon.ini'), systemId) ||
    readFromXmlFile(path.join(APPDATA, 'SAP/Common/SAPUILandscape.xml'), systemId) ||
    readFromXmlFile(path.join(APPDATA, 'SAP/Common/SAPUILandscapeGlobal.xml'), systemId);

  // try to load server config from cache files
  if (!config) {
    var cacheDir = path.join(APPDATA, 'SAP/LogonServerConfigCache');
    if (fs.existsSync(cacheDir)) {
      var cacheFiles = fs.readdirSync(cacheDir).filter(function(filepath) {
        return path.extname(filepath).toLowerCase() === '.xml';
      });
  
      for (var i = 0; i < cacheFiles.length; ++i) {
        config = readFromXmlFile(path.join(cacheDir, cacheFiles[i]), systemId);
        if (config) break;
      }
    }
  }

  if (!config) {
    return null;
  }
  
  var port = '80' + config.database;
  var host = config.server + ':' + port;
  var url = 'http://' + host + '/';

  return {
    server: config.server,
    database: config.database,
    description: config.description,
    port: port,
    host: host,
    hostname: config.server,
    url: url
  };
};
