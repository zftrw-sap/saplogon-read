# saplogon-read

Access SAP Logon connection data from Node.js


## Installation

```
$ npm install saplogon-read
```


## Usage
```js
const saplogon = require('saplogon-read');

const logon = saplogon('BT0');  // <--- use your system name

console.log('Server', logon.server);           // example.org
console.log('Database', logon.database);       // 04
console.log('Description', logon.description); // BT0 - Example Development Server
console.log('Host', logon.host);               // example.org:8004
console.log('Port', logon.port);               // 8004 
console.log('Hostname', logon.hostname);       // example.org
console.log('URL', logon.url);                 // http://example.org:8004/
```

`saplogon-read` tries to load configuration data from following locations:
```
%LOCALAPPDATA%/SAP/Common/saplogon.ini
%APPDATA%/SAP/Common/saplogon.ini
%APPDATA%/SAP/Common/SAPUILandscape.xml
%APPDATA%/SAP/Common/SAPUILandscapeGlobal.xml
%APPDATA%/SAP/LogonServerConfigCache/*.xml
```

`saplogon-read` returns `null` if none of above files contains requested system information.

If you are running node from WSL (Windows Subsystem for Linux) in Windows 10, make sure `APPDATA` or `LOCALAPPDATA`
environment variables are available to the node process. Just add these lines to `~/.bashrc` or `~/.bash_aliases`:

```bash
export LOCALAPPDATA=/mnt/c/Users/YOURUSERNAME/AppData/Local
export APPDATA=/mnt/c/Users/YOURUSERNAME/AppData/Roaming
```


## License

MIT
